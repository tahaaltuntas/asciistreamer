#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>


static dev_t asciistreamer_devt; // Global variable for the first device number
static struct cdev asciistreamer_cdev; // Global variable for the character device structure
static struct class *cl; // Global variable for the device class

static char* data;

static int asciistreamer_open(struct inode *i, struct file *f) {
    printk(KERN_INFO "Driver: open()\n");
    return 0;
}

static int asciistreamer_close(struct inode *i, struct file *f) {
    printk(KERN_INFO "Driver: close()\n");
    return 0;
}
static ssize_t asciistreamer_read(struct file *f, char __user *buf, size_t len, loff_t *off) {
    printk(KERN_INFO "Driver: read()\n");
    return 0;
}

static ssize_t asciistreamer_write(struct file *f, const char __user *buf, size_t len, loff_t *off) {
    printk(KERN_INFO "Driver: write()\n");
    char mal[6];

    copy_from_user(mal, buf, 6);
    mal[5] = '\0';
    printk(KERN_INFO "Given string: %s", mal);
    return len;
}
static struct file_operations asciistreamer_fops = {
    .owner = THIS_MODULE,
    .open = asciistreamer_open,
    .release = asciistreamer_close,
    .read = asciistreamer_read,
    .write = asciistreamer_write
};

static int __init asciistreamer_init(void) {
    if (alloc_chrdev_region(&asciistreamer_devt, 0, 1, "asciistreamer") < 0) {
        return -1;
    }

    if ((cl = class_create(THIS_MODULE, "asciistreamer")) == NULL) {
        unregister_chrdev_region(asciistreamer_devt, 1);
        return -1;
    }

    if (device_create(cl, NULL, asciistreamer_devt, NULL, "asciistreamer") == NULL) {
        class_destroy(cl);
        unregister_chrdev_region(asciistreamer_devt, 1);
        return -1;
    }

    cdev_init(&asciistreamer_cdev, &asciistreamer_fops);

    if (cdev_add(&asciistreamer_cdev, asciistreamer_devt, 1) == -1) {
        device_destroy(cl, asciistreamer_devt);
        class_destroy(cl);
        unregister_chrdev_region(asciistreamer_devt, 1);
        return -1;
    }

    printk(KERN_INFO "AsciiStreamer is registered.");
    return 0;
}

static void __exit asciistreamer_exit(void) {
    cdev_del(&asciistreamer_cdev);
    device_destroy(cl, asciistreamer_devt);
    class_destroy(cl);
    unregister_chrdev_region(asciistreamer_devt, 1);
    printk(KERN_INFO "AsciiStreamer is unregistered.");
}

module_init(asciistreamer_init);
module_exit(asciistreamer_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Taha Altuntas");
MODULE_DESCRIPTION("AsciiStreamer device driver.");
